/*
*/
CREATE PROCEDURE [dbo].[spMarineMCI_Cancellation_Select]

AS

Begin

   SELECT LogNo,ItmUk,RowID
       ,[Request_number]
      ,[Ver_no]
      ,[Importer_PIN]
      ,[Importer_Type]
      ,[Broker_Name]
      ,[Broker_PIN]
      ,[Ucr_code]
      ,[Origin_country]
      ,[Destination_country]
      ,[Origin_port]
      ,[Destination_port]
      ,[Mci_Issuer]
      ,[Rotation_Number]
      ,[Vessel_Name]
      ,[Voyage_Number]
	  ,[eta]
      ,[dry_bulk_ind]
      ,[cont_ind]
      ,[general_ind]
      ,[vehicle_ind]
      ,[animal_ind]
      ,[liquid_ind]
      ,[Financier_PIN]
      ,[Tran_port]
      ,[Tran_country] as country
      ,[Tran_quantity] as quantity
      ,[Tran_vessel] as vessel_name
      ,[itemSeq_num]
      ,[goods_desc]
      ,[quantity] as itm_quantity
      ,[uom]
      ,[package_type]
      ,[Marks_Numbers]
      ,[item_Origin_country] as origin_country
      ,[item_currency] as currency
      ,[CIF]
      ,[CIF_NCY]
      ,[Doc_name]
      ,[Doc_code]
      ,[File_Name]
      ,[Mci_internal_number]
      ,[Mci_certificate_number]
      ,[Policy_Number]
      ,[Premium_amount]
      ,[Policy_holders_fund]
      ,[Stamp_duty]
      ,[Training_levy]
      ,[Total_premium]
      ,[Sum_Insured]
      ,[Servey_agents]
      ,[approval_type]
      ,[role_code]
      ,[Remarks]
      ,[Status]
      ,[Created_date]
      ,[Transferee_PIN]
      ,[Transferee_UCR]
      ,[SendAttempts]
      ,[Updated]
	  ,[Request_Type]
  FROM [ESBMALL].[dbo].[IceaLionMarine] where Updated=0 and (UCR_Validation is null or UCR_Validation='' or UCR_Validation='fail') and Ver_no=3

end

go

