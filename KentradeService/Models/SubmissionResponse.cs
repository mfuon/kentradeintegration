using Newtonsoft.Json;

namespace KenTradeService.Models
{

    namespace JSONUtils
    {

        public class Xml
        {

            [JsonProperty("@version")] public string @version { get; set; }

            [JsonProperty("@encoding")] public string @encoding { get; set; }
        }

        public class Msghdr
        {

            [JsonProperty("msgid")] public string msgid { get; set; }

            [JsonProperty("refno")] public string refno { get; set; }

            [JsonProperty("msg_func")] public string msg_func { get; set; }

            [JsonProperty("sender")] public string sender { get; set; }

            [JsonProperty("receiver")] public string receiver { get; set; }

            [JsonProperty("version")] public string version { get; set; }

            [JsonProperty("docno")] public string docno { get; set; }

            [JsonProperty("docgroup")] public string docgroup { get; set; }

            [JsonProperty("msgdate")] public string msgdate { get; set; }
        }

        public class Response
        {

            [JsonProperty("code")] public string code { get; set; }

            [JsonProperty("description")] public string description { get; set; }
        }

        public class Error
        {

            [JsonProperty("code")] public string code { get; set; }

            [JsonProperty("description")] public string description { get; set; }
        }

        public class Errors
        {

            [JsonProperty("error")] public Error error { get; set; }
        }

        public class MciResponse
        {

            [JsonProperty("response")] public Response response { get; set; }

            [JsonProperty("errors")] public Errors errors { get; set; }
        }

        public class Mcires
        {

            [JsonProperty("msghdr")] public Msghdr msghdr { get; set; }

            [JsonProperty("mci_response")] public MciResponse mci_response { get; set; }
        }

        public class SubmissionResponse
        {

            [JsonProperty("?xml")] public Xml xml { get; set; }

            [JsonProperty("mcires")] public Mcires mcires { get; set; }
        }
    }
}