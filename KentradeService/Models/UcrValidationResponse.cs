using Newtonsoft.Json;

namespace KenTradeService.Models
{
    
    public class DocumentHeader
    {

        [JsonProperty("msgid")]
        public string msgid { get; set; }

        [JsonProperty("refno")] public string refno { get; set; }

        [JsonProperty("msg_func")] public string msg_func { get; set; }

        [JsonProperty("sender")] public string sender { get; set; }

        [JsonProperty("receiver")] public string receiver { get; set; }

        [JsonProperty("version")] public string version { get; set; }

        [JsonProperty("docno")] public string docno { get; set; }

        [JsonProperty("docgroup")] public string docgroup { get; set; }

        [JsonProperty("msgdate")]
        public string msgdate { get; set; }
    }

    public class DocumentDetails
    {

        [JsonProperty("ucr_no")]
        public string ucr_no { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("ImporterPIN")]
        public string ImporterPIN { get; set; }

        [JsonProperty("ImporterName")]
        public string ImporterName { get; set; }

        [JsonProperty("ExporterPIN")]
        public string ExporterPIN { get; set; }

        [JsonProperty("ExporterName")]
        public string ExporterName { get; set; }

        [JsonProperty("CustomsEntryNumber")]
        public string CustomsEntryNumber { get; set; }

        [JsonProperty("partshipmentInd")]
        public string partshipmentInd { get; set; }

        [JsonProperty("parentUCR")]
        public string parentUCR { get; set; }
    }

    public class UcrValidationResponse
    {

        [JsonProperty("DocumentHeader")]
        public DocumentHeader DocumentHeader { get; set; }

        [JsonProperty("DocumentDetails")]
        public DocumentDetails DocumentDetails { get; set; }
    }

}