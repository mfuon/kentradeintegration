namespace KenTradeService.Models
{
    public class IceaLionMarine
    {
        public long RowID { get; set; }
        public long? LogNo { get; set; }
        public long? ItmUk { get; set; }
        public string Request_number { get; set; }
        public decimal? Ver_no { get; set; }
        public string Broker_Name { get; set; }
        public string Ucr_code { get; set; }
        public string msgid { get; set; }
        public string msg_func { get; set; }
        public string sender { get; set; }
        public string receiver { get; set; }
        public string version { get; set; }
        public string ucr_no { get; set; }
    }
}