namespace KenTradeService.Models
{
    public class IceaLionMarineSubmission
    {
        public long RowID { get; set; }
        public long? LogNo { get; set; }
        public long? ItmUk { get; set; }
        public string Request_number { get; set; }
        public decimal? Ver_no { get; set; }
        public string Importer_Type { get; set; }
        public string Broker_Name { get; set; }
        public string Broker_PIN { get; set; }
        public string Ucr_code { get; set; }
        public string Origin_country { get; set; }
        public string Destination_country { get; set; }
        public string Origin_port { get; set; }
        public string Destination_port { get; set; }
        public string Mci_Issuer { get; set; }
        public string Rotation_Number { get; set; }
        public string Vessel_Name { get; set; }
        public string Voyage_Number { get; set; }
        public string dry_bulk_ind { get; set; }
        public string cont_ind { get; set; }
        public string general_ind { get; set; }
        public string vehicle_ind { get; set; }
        public string animal_ind { get; set; }
        public string liquid_ind { get; set; }
        public string eta { get; set; }
        public string Financier_PIN { get; set; }
        public string Tran_port { get; set; }
        public string Tran_country { get; set; }
        public double? Tran_quantity { get; set; }
        public string Tran_vessel { get; set; }
        public int? itemSeq_num { get; set; }
        public string goods_desc { get; set; }
        public int? quantity { get; set; }
        public string uom { get; set; }
        public string package_type { get; set; }
        public string Marks_Numbers { get; set; }
        public string item_Origin_country { get; set; }
        public string item_currency { get; set; }
        public double? CIF { get; set; }
        public double? CIF_NCY { get; set; }
        public string Doc_name { get; set; }
        public string Doc_code { get; set; }
        public string File_Name { get; set; }
        public string Mci_internal_number { get; set; }
        public string Mci_certificate_number { get; set; }
        public string Policy_Number { get; set; }
        public decimal? Premium_amount { get; set; }
        public decimal? Policy_holders_fund { get; set; }
        public decimal? Stamp_duty { get; set; }
        public decimal? Training_levy { get; set; }
        public decimal? Total_premium { get; set; }
        public decimal? Sum_Insured { get; set; }
        public string Servey_agents { get; set; }
        public string approval_type { get; set; }
        public string role_code { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public string Created_date { get; set; }
        public string Transferee_PIN { get; set; }
        public string Transferee_UCR { get; set; }
        public string ucr_no { get; set; }
     
        public string ImporterPIN { get; set; }
        
    }
}