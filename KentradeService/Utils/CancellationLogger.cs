using System;
using System.IO;
using KenTradeService.Utils;

namespace KentradeService.Utils
{
    public static class CancellationLogger
    {
        public static void Log(Exception ex)
        {
            try
            {
                Constants.CreateDirectoryLogs();
                var sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LOGS\\A_CANCELLATION_ERROR_LOG.txt", true);
                sw.WriteLine(DateTime.Now + " : " + ex.Source.Trim() + "; >> " + ex.Message.Trim() + "  >> TRACE >> " + ex.StackTrace);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void Log(string message)
        {
            try
            {
                Constants.CreateDirectoryLogs();
                var sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LOGS\\A_CANCELLATION_MESSAGE_LOG.txt", true);
                sw.WriteLine(DateTime.Now + " ====>>> " + message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
    }
}