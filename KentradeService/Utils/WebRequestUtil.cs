using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using KentradeService.Utils;
using KenTradeService.Models;

namespace KenTradeService.Utils
{
    public static class WebRequestUtil
    {
        public static HttpWebRequest CreateWebRequest(string url, string action)
        {
            var webRequest = (HttpWebRequest) WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
        
        public static HttpWebRequest CreateSubmissionWebRequest(string url, string action)
        {
            var webRequest = (HttpWebRequest) WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
        
        public static HttpWebRequest CreateCancellationWebRequest(string url, string action)
        {
            var webRequest = (HttpWebRequest) WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        public static XmlDocument CreateUcrValidationSoapEnvelope(IceaLionMarine marine, string ucrRequestToken)
        {
            var soapEnvelopeDocument = new XmlDocument();
            XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace web = "http://webservice.ucr.oga.kesws.crimsonlogic.com/";
            var root = new XElement(soapenv + "Envelope",
                new XAttribute(XNamespace.Xmlns + "soapenv", soapenv.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "web", web.NamespaceName),
                new XElement(
                    soapenv + "Body",
                    new XElement(web + "ucrValidation",
                        new XElement("arg0",
                            new XCData("<UCR_Enquiry><DocumentHeader>" +
                                       "<msgid>" + marine.msgid + "</msgid>" +
                                       "<refno>" + marine.Ucr_code + "</refno>" +
                                       "<msg_func>" + marine.msg_func + "</msg_func>" +
                                       "<sender>" + marine.sender + "</sender>" +
                                       "<receiver>" + marine.receiver + "</receiver>" +
                                       "<version>" + marine.version + "</version>" +
                                       "</DocumentHeader>" +
                                       "<DocumentDetails>" +
                                       "<ucr_no>" + marine.ucr_no + "</ucr_no>" +
                                       "<token>" + ucrRequestToken + "</token>" +
                                       "</DocumentDetails>" +
                                       "</UCR_Enquiry>")
                        )
                    )
                )
            );
            soapEnvelopeDocument.LoadXml(root.ToString());
            return soapEnvelopeDocument;
        }
        public static XmlDocument CreateUcrSubmissionSoapEnvelope(IceaLionMarineSubmission m, string mciRequestToken)
        {
            var envelopeelopeDocument = new XmlDocument();
            var root = "<?xml version='1.0' encoding='utf-8'?>" +
                       "<Envelope xmlns='http://schemas.xmlsoap.org/soap/envelope/'>" +
                       "<Body>" +
                       "<receiveMCISubmissionRequest xmlns='http://service.mci.kesws.crimsonlogic.com/'>" +
                       "<arg0 xmlns=''>" +
                       "<![CDATA[ " +
                       "<?xml version='1.0' encoding='utf-8'?>" +
                       "<mcirequest> " +
                       "<msghdr> " +
                       "<msgid>OG_MCI_REQ</msgid> " +
                       "<refno>"+m.RowID+"</refno> " +
                       "<msg_func>9</msg_func> " +
                       "<sender>ION</sender> " +
                       "<receiver>KESWS</receiver> " +
                       "<version>1</version> " +
                       "<docno>"+m.Doc_code+"</docno> <docgroup>"+m.Doc_name+"</docgroup> " +
                       "<msgdate>"+m.Created_date+"</msgdate> " +
                       "<token>"+mciRequestToken+"</token> " +
                       "</msghdr> " +
                       "" +
                       "<mci_details> " +
                       "<request_number>"+m.Request_number+"</request_number> " +
                       "<ver_no>"+m.Ver_no+"</ver_no> " +
                       "<importer_pin>"+m.ImporterPIN+"</importer_pin> " +
                       "<importer_type>"+m.Importer_Type+"</importer_type> " +
                       "<broker_name>"+m.Broker_Name+"</broker_name> " +
                       "<broker_pin>"+m.Broker_PIN+"</broker_pin> " +
                       "<ucr_code>"+m.Ucr_code+"</ucr_code> " +
                       "<origin_country>"+m.Origin_country+"</origin_country> " +
                       "<destination_country>"+m.Destination_country+"</destination_country>" +
                       "<origin_port>"+m.Origin_port+"</origin_port> " +
                       "<destination_port>"+m.Destination_port+"</destination_port> " +
                       "<mci_issuer>"+m.Mci_Issuer+"</mci_issuer> " +
                       "<rotation_number>"+m.Rotation_Number+"</rotation_number> " +
                       "<vessel_name>"+m.Vessel_Name+"</vessel_name> " +
                       "<voyage_number>"+m.Voyage_Number+"</voyage_number> " +
                       "<eta>"+m.eta+"</eta> " +
                       "" +
                       "<cargoType> " +
                       "<dry_bulk_ind>"+m.dry_bulk_ind+"</dry_bulk_ind> " +
                       "<cont_ind>"+m.cont_ind+"</cont_ind> " +
                       "<general_ind>"+m.general_ind+"</general_ind> " +
                       "<vehicle_ind>"+m.vehicle_ind+"</vehicle_ind> " +
                       "<animal_ind>"+m.animal_ind+"</animal_ind> " +
                       "<liquid_ind>"+m.liquid_ind+"</liquid_ind> " +
                       "</cargoType> " +
                       "" +
                       "<financier_pin>"+m.Financier_PIN+"</financier_pin> " +
                       "" +
                       "<transhipping>" +
                       "<trans_port>"+m.Tran_port+"</trans_port> " +
                       "<country>"+m.Tran_country+"</country> " +
                       "<quantity>"+m.Tran_quantity+"</quantity> " +
                       "<vessel_name>"+m.Tran_vessel+"</vessel_name> " +
                       "</transhipping> " +
                       "<items> " +
                       "<itemSeq_num>"+m.itemSeq_num+"</itemSeq_num> " +
                       "<goods_desc>"+m.goods_desc+"</goods_desc> " +
                       "<quantity>"+m.quantity+"</quantity> " +
                       "<uom>"+m.uom+"</uom> " +
                       "<package_type>"+m.package_type+"</package_type> " +
                       "<marks_numbers>"+m.Marks_Numbers+"</marks_numbers> " +
                       "<origin_country>"+m.Origin_country+"</origin_country> " +
                       "<currency_code>"+m.item_currency+"</currency_code> " +
                       "<CIF>"+m.CIF+"</CIF> " +
                       "<CIF_NCY>"+m.CIF_NCY+"</CIF_NCY> " +
                       "</items> " +
                       "" +
                       "<documents> " +
                       "<doc_name>"+m.Doc_name+"</doc_name> " +
                       "<doc_code>"+m.Doc_code+"</doc_code> " +
                       "<file_name>"+m.File_Name+"</file_name> " +
                       "</documents> " +
                       "<mci_internal_number>"+m.Mci_internal_number+"</mci_internal_number> " +
                       "<mci_certificate_number>"+m.Mci_certificate_number+"</mci_certificate_number> " +
                       "<policy_number>"+m.Policy_Number+"</policy_number> " +
                       "<premium_amount>"+m.Premium_amount+"</premium_amount> " +
                       "<policy_holders_fund>"+m.Policy_holders_fund+"</policy_holders_fund> " +
                       "<stamp_duty>"+m.Stamp_duty+"</stamp_duty> " +
                       "<training_levy>"+m.Training_levy+"</training_levy> " +
                       "<total_premium>"+m.Total_premium+"</total_premium> " +
                       "<sum_insured>"+m.Sum_Insured+"</sum_insured> " +
                       "<servey_agents>"+m.Servey_agents+"</servey_agents> " +
                       "<approval_type>"+m.approval_type+"</approval_type> " +
                       "<role_code>"+m.role_code+"</role_code> " +
                       "<remarks>"+m.Remarks+"</remarks> " +
                       "<status>"+m.Status+"</status> " +
                       "<created_date>"+m.Created_date+"</created_date> " +
                       "</mci_details> " +
                       "</mcirequest> ]]> " +
                       "</arg0>" +
                       "</receiveMCISubmissionRequest>" +
                       "</Body>" +
                       "</Envelope>";
            envelopeelopeDocument.LoadXml(root);
            SubmissionLogger.Log(
                "****** XML ******* " + root);
            return envelopeelopeDocument;
        }
        public static XmlDocument CreateUcrAmendmentSoapEnvelope(IceaLionMarineSubmission m, string mciRequestToken,string refNumber)
        {
            var envelopeelopeDocument = new XmlDocument();
            var root = "<?xml version='1.0' encoding='utf-8'?>" +
                       "<Envelope xmlns='http://schemas.xmlsoap.org/soap/envelope/'>" +
                       "<Body>" +
                       "<receiveMCISubmissionRequest xmlns='http://service.mci.kesws.crimsonlogic.com/'>" +
                       "<arg0 xmlns=''>" +
                       "<![CDATA[ " +
                       "<?xml version='1.0' encoding='utf-8'?>" +
                       "<mcirequest> " +
                       "<msghdr> " +
                       "<msgid>OG_MCI_REQ</msgid> " +
                       "<refno>"+m.RowID+"</refno> " +
                       "<msg_func>5</msg_func> " +
                       "<sender>ION</sender> " +
                       "<receiver>KESWS</receiver> " +
                       "<version>2</version> " +
                       "<docno>"+m.Doc_code+"</docno> <docgroup>"+m.Doc_name+"</docgroup> " +
                       "<msgdate>"+m.Created_date+"</msgdate> " +
                       "<token>"+mciRequestToken+"</token> " +
                       "</msghdr> " +
                       "" +
                       "<mci_details> " +
                       "<request_number>"+refNumber+"</request_number> " +
                       "<ver_no>"+m.Ver_no+"</ver_no> " +
                       "<importer_pin>"+m.ImporterPIN+"</importer_pin> " +
                       "<importer_type>"+m.Importer_Type+"</importer_type> " +
                       "<broker_name>"+m.Broker_Name+"</broker_name> " +
                       "<broker_pin>"+m.Broker_PIN+"</broker_pin> " +
                       "<ucr_code>"+m.Ucr_code+"</ucr_code> " +
                       "<origin_country>"+m.Origin_country+"</origin_country> " +
                       "<destination_country>"+m.Destination_country+"</destination_country>" +
                       "<origin_port>"+m.Origin_port+"</origin_port> " +
                       "<destination_port>"+m.Destination_port+"</destination_port> " +
                       "<mci_issuer>"+m.Mci_Issuer+"</mci_issuer> " +
                       "<rotation_number>"+m.Rotation_Number+"</rotation_number> " +
                       "<vessel_name>"+m.Vessel_Name+"</vessel_name> " +
                       "<voyage_number>"+m.Voyage_Number+"</voyage_number> " +
                       "<eta>"+m.eta+"</eta> " +
                       "" +
                       "<cargoType> " +
                       "<dry_bulk_ind>"+m.dry_bulk_ind+"</dry_bulk_ind> " +
                       "<cont_ind>"+m.cont_ind+"</cont_ind> " +
                       "<general_ind>"+m.general_ind+"</general_ind> " +
                       "<vehicle_ind>"+m.vehicle_ind+"</vehicle_ind> " +
                       "<animal_ind>"+m.animal_ind+"</animal_ind> " +
                       "<liquid_ind>"+m.liquid_ind+"</liquid_ind> " +
                       "</cargoType> " +
                       "" +
                       "<financier_pin>"+m.Financier_PIN+"</financier_pin> " +
                       "" +
                       "<transhipping> " +
                       " <trans_port>"+m.Tran_port+"</trans_port> " +
                       "<country>"+m.Tran_country+"</country> " +
                       "<quantity>"+m.Tran_quantity+"</quantity> " +
                       "<vessel_name>"+m.Tran_vessel+"</vessel_name> " +
                       "</transhipping> " +
                       "<items> " +
                       "<itemSeq_num>"+m.itemSeq_num+"</itemSeq_num> " +
                       "<goods_desc>"+m.goods_desc+"</goods_desc> " +
                       "<quantity>"+m.quantity+"</quantity> " +
                       "<uom>"+m.uom+"</uom> " +
                       "<package_type>"+m.package_type+"</package_type> " +
                       "<marks_numbers>"+m.Marks_Numbers+"</marks_numbers> " +
                       "<origin_country>"+m.Origin_country+"</origin_country> " +
                       "<currency_code>"+m.item_currency+"</currency_code> " +
                       "<CIF>"+m.CIF+"</CIF> " +
                       "<CIF_NCY>"+m.CIF_NCY+"</CIF_NCY> " +
                       "</items> " +
                       "" +
                       "<documents> " +
                       "<doc_name>"+m.Doc_name+"</doc_name> " +
                       "<doc_code>"+m.Doc_code+"</doc_code> " +
                       "<file_name>"+m.File_Name+"</file_name> " +
                       "</documents> " +
                       "<mci_internal_number>"+m.Mci_internal_number+"</mci_internal_number> " +
                       "<mci_certificate_number>"+m.Mci_certificate_number+"</mci_certificate_number> " +
                       "<policy_number>"+m.Policy_Number+"</policy_number> " +
                       "<premium_amount>"+m.Premium_amount+"</premium_amount> " +
                       "<policy_holders_fund>"+m.Policy_holders_fund+"</policy_holders_fund> " +
                       "<stamp_duty>"+m.Stamp_duty+"</stamp_duty> " +
                       "<training_levy>"+m.Training_levy+"</training_levy> " +
                       "<total_premium>"+m.Total_premium+"</total_premium> " +
                       "<sum_insured>"+m.Sum_Insured+"</sum_insured> " +
                       "<servey_agents>"+m.Servey_agents+"</servey_agents> " +
                       "<approval_type>"+m.approval_type+"</approval_type> " +
                       "<role_code>"+m.role_code+"</role_code> " +
                       "<remarks>"+m.Remarks+"</remarks> " +
                       "<status>"+m.Status+"</status> " +
                       "<created_date>"+m.Created_date+"</created_date> " +
                       "" +
                       "<endrosement> " +
                       "<transferee_pin></transferee_pin><transferee_ucr></transferee_ucr> " +
                       "</endrosement> " +
                       "</mci_details> " +
                       "</mcirequest> ]]> " +
                       "</arg0>" +
                       "</receiveMCISubmissionRequest>" +
                       "</Body>" +
                       "</Envelope>";
            envelopeelopeDocument.LoadXml(root);
           AmendmentLogger.Log(
                "****** XML ******* " + root);
            return envelopeelopeDocument;
        }
        public static XmlDocument CreateUcrCancellationSoapEnvelope(IceaLionMarineSubmission m, string mciRequestToken,string refNumber)
        {
            var envelopeDocument = new XmlDocument();
            var root = "<?xml version='1.0' encoding='UTF-8'?>" +
                       "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ser='http://service.mci.kesws.crimsonlogic.com/'>" +
                       "<soapenv:Header />" +
                       "<soapenv:Body>" +
                       "<ser:receiveMCICancelSubServiceReq>" +
                       "<!--Optional:-->" +
                       "<arg0><![CDATA[<?xml version='1.0' encoding='utf-8'?>" +
                       "<mci_cancel_request>" +
                       "<msghdr>" +
                       "<msgid>MC_CAN_REQ</msgid>" +
                       "<refno>"+refNumber+"</refno>" +
                       "<msg_func>9</msg_func>" +
                       "<sender>ION</sender>" +
                       "<receiver>KESWS</receiver>" +
                       "<version>1</version>" +
                       "<docno>169</docno>" +
                       "<docgroup>str1234</docgroup>" +
                       "<msgdate>"+m.Created_date+"</msgdate>" +
                       "<token>"+mciRequestToken+"</token>" +
                       "</msghdr>" +
                       "<mci_details>" +
                       "<MCI_request_number>"+refNumber+"</MCI_request_number>" +
                       "<mci_cert_number>"+m.Mci_certificate_number+"</mci_cert_number>" +
                       "<status>Cancellation_Approved</status>" +
                       "<cancel_reason_code>213</cancel_reason_code>" +
                       "<remarks>Cancelled</remarks>" +
                       "<role_code>R_BA_PCO</role_code>" +
                       "<submitted_date>"+m.Created_date+"</submitted_date>" +
                       "</mci_details>" +
                       "</mci_cancel_request>]]>" +
                       "&gt;</arg0>" +
                       "</ser:receiveMCICancelSubServiceReq>" +
                       "</soapenv:Body>" +
                       "</soapenv:Envelope>";
            envelopeDocument.LoadXml(root);
            CancellationLogger.Log("****** XML ******* " + root);
            return envelopeDocument;
        }

        public static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (var stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
        
        public static void InsertSubmissionSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
                using (var stream = webRequest.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }
        }
        
        public static void InsertCancellationSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (var stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }

        public static string sha256_hash(string value)
        {
            using (var hash = SHA256.Create())
            {
                return String.Concat(hash
                    .ComputeHash(Encoding.UTF8.GetBytes(value))
                    .Select(item => item.ToString("x2")));
            }
        }
    }
}