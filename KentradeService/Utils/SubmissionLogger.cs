﻿using System;
using System.IO;
using KenTradeService.Utils;

namespace KentradeService.Utils
{
    public static class SubmissionLogger
    {
        public static void Log(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                Constants.CreateDirectoryLogs();
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LOGS\\A_SUBMISSION_ERROR_LOG.txt", true);
                sw.WriteLine(DateTime.Now + " : " + ex.Source.Trim() + "; >> " + ex.Message.Trim() + "  >> TRACE >> " + ex.StackTrace);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void Log(string message)
        {
            StreamWriter sw = null;
            try
            {
                Constants.CreateDirectoryLogs();
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LOGS\\A_SUBMISSION_MESSAGE_LOG.txt", true);
                sw.WriteLine(DateTime.Now + " ====>>> " + message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
    }
}