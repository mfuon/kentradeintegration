﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace KentradeService.Utils
{
    public static class Connection
    {
        public static SqlConnection GetConnections() {
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ESBMALL"].ConnectionString);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                SubmissionLogger.Log(ex);
            }

            return connection;
        }
    }
}
