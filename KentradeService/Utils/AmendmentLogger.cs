﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace KenTradeService.Utils
{
    public static class AmendmentLogger
    {
        public static void Log(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                Constants.CreateDirectoryLogs();
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LOGS\\A_AMENDMENT_ERROR_LOG.txt", true);
                sw.WriteLine(DateTime.Now + " : " + ex.Source.Trim() + "; >> " + ex.Message.Trim() + "  >> TRACE >> " + ex.StackTrace);
                sw.Flush();
                sw.Close();
            }
            catch(IOException e)
            {
                Log(e);
            }
        }

        public static void Log(string message)
        {
            StreamWriter sw = null;
            try
            {
                Constants.CreateDirectoryLogs();
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LOGS\\A_AMENDMENT_MESSAGE_LOG.txt", true);
                sw.WriteLine(DateTime.Now + " =====> " + message);
                sw.Flush();
                sw.Close();
            }
            catch(IOException e)
            {
                Log(e);
            }
        }
    }
}