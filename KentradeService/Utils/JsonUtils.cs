using System.Xml;
using KentradeService.Utils;
using KenTradeService.Models;
using KenTradeService.Models.JSONUtils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KenTradeService.Utils
{
    public class JsonUtils
    {
        public static SubmissionResponse GetSubmissionResponse(string soapResultForSubmission)
        {
            var submissionDoc = new XmlDocument();
            submissionDoc.LoadXml(soapResultForSubmission);
            var submissionJson = JsonConvert.SerializeXmlNode(submissionDoc);
            var obj_ = JObject.Parse(submissionJson);
            SubmissionLogger.Log("****** JSON ******* " + obj_);
            var token_ =obj_["S:Envelope"]["S:Body"]["ns0:receiveMCISubmissionRequestResponse"]["return"];
            submissionDoc.LoadXml(token_.ToString());
            var json2_ = JsonConvert.SerializeXmlNode(submissionDoc);
            var obj2_ = JObject.Parse(json2_);
            SubmissionLogger.Log("****** JSON ******* " + obj2_);
            var response_ = JsonConvert.DeserializeObject<SubmissionResponse>(obj2_.ToString());
            SubmissionLogger.Log("****** CODE ******* " + response_.mcires.mci_response.errors.error.code);
            return response_;
        }
        public static SubmissionResponse GetAmendmentResponse(string soapResultForSubmission)
        {
            AmendmentLogger.Log(Constants.Lines());
            var submissionDoc = new XmlDocument();
            submissionDoc.LoadXml(soapResultForSubmission);
            var submissionJson = JsonConvert.SerializeXmlNode(submissionDoc);
            var obj_ = JObject.Parse(submissionJson);
            AmendmentLogger.Log("****** JSON RESPONSE ******* " + obj_);
            var token_ =obj_["S:Envelope"]["S:Body"]["ns0:receiveMCISubmissionRequestResponse"]["return"];
            submissionDoc.LoadXml(token_.ToString());
            var json2_ = JsonConvert.SerializeXmlNode(submissionDoc);
            var obj2_ = JObject.Parse(json2_);
            AmendmentLogger.Log("****** JSON RESPONSE******* " + obj2_);
            var response_ = JsonConvert.DeserializeObject<SubmissionResponse>(obj2_.ToString());
            AmendmentLogger.Log(Constants.Lines());
            return response_;
        }
        public static UcrValidationResponse GetValidationResponse(string soapResult)
        {
            var doc = new XmlDocument();
            doc.LoadXml(soapResult);
            var json = JsonConvert.SerializeXmlNode(doc);
            var obj = JObject.Parse(json);
            var token = obj["S:Envelope"]["S:Body"]["ns0:ucrValidationResponse"]["return"];
            doc.LoadXml(token.ToString());
            var json2 = JsonConvert.SerializeXmlNode(doc);
            var obj2 = JObject.Parse(json2);
            var jsonInput = obj2["UCR_Response"].ToString();
            var response = JsonConvert.DeserializeObject<UcrValidationResponse>(jsonInput);
            return response;
        }
      
    }
}