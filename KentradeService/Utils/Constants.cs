using System;
using System.Configuration;
using System.IO;
using KentradeService.Utils;

namespace KenTradeService.Utils
{
    public static class Constants
    {
        //validation
        /*public const string Url = "https://trial.kenyatradenet.go.ke/kesws/UCRValidationWebService";
        public const string Action = "ucrValidation";*/
        public static readonly string Url = GetSetting("validationUrl","https://trial.kenyatradenet.go.ke/kesws/UCRValidationWebService");
        public static readonly string Action = GetSetting("validationAction","ucrValidation");
        
        //Submission
        //public const string SubmissionUrl = "https://trial.kenyatradenet.go.ke/kesws/MCISubmissionService";
        public static readonly string SubmissionUrl = GetSetting("submissionAndAmendmentUrl","https://trial.kenyatradenet.go.ke/kesws/MCISubmissionService");
        public static readonly string SubmissionAction = GetSetting("submissionAndAmendmentAction","receiveMCISubmissionRequest");

        //cancellation
        //public const string CancellationUrl = "http://trial.kenyatradenet.go.ke/kesws/MCICancelSubService";
        public static readonly string CancellationUrl = GetSetting("cancellationUrl","http://trial.kenyatradenet.go.ke/kesws/MCICancelSubService");
        public static readonly string CancellationAction = GetSetting("cancellationAction","receiveMCICancelSubServiceReq");
        
        //Frequency to how the services run
        public static readonly int ServiceTimer = GetSetting("timer",21600000); //THE DEFAULT VALUE IS 30 MINUTES
        
        //LOGS Folder
        private static readonly string FilePath = AppDomain.CurrentDomain.BaseDirectory + "\\LOGS\\";

        public static string Lines()
        {
            return
                "==================================================================================================================";
        }

        public static void Initializer()
        {
            SubmissionLogger.Log(">>>>>>>KENTRADE SERVICE STARTED<<<<<<<<<  " + DateTime.Now);
            CancellationLogger.Log(">>>>>>>KENTRADE SERVICE STARTED<<<<<<<<<  " + DateTime.Now);
            AmendmentLogger.Log(">>>>>>>KENTRADE SERVICE STARTED<<<<<<<<<  " + DateTime.Now);
        }

        public static void End()
        {
            SubmissionLogger.Log(">>>>>>>Kentrade Service Stopped<<<<<<<<  " + DateTime.Now);
            AmendmentLogger.Log(">>>>>>>Kentrade Service Stopped<<<<<<<<<  " + DateTime.Now);
            CancellationLogger.Log(">>>>>>>Kentrade Service Stopped<<<<<<<<<  " + DateTime.Now);
        }
        
        public static T GetSetting<T>(string key, T defaultValue = default(T)) where T : IConvertible  
        {  
            string val = ConfigurationManager.AppSettings[key] ?? "";  
            T result = defaultValue;  
            if (!string.IsNullOrEmpty(val))  
            {  
                T typeDefault = default(T);  
                if (typeof(T) == typeof(String))  
                {  
                    typeDefault = (T)(object)String.Empty;  
                }

                if (typeDefault != null) result = (T) Convert.ChangeType(val, typeDefault.GetTypeCode());
            }  
            return result;  
        }

        public static void CreateDirectoryLogs()
        {
            if (!Directory.Exists(FilePath))
            {
                Directory.CreateDirectory(FilePath);
            }
        
        }
        
        
    }
}