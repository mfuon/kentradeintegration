using System;
using System.Data;
using System.Data.SqlClient;
using KentradeService.Utils;
using KenTradeService.Models;
using KenTradeService.Models.JSONUtils;

namespace KenTradeService.Data
{
    public static class DbOperations
    {
        public static void UpdateFailureOperation(long id)
        {
            var failureConnection = Connection.GetConnections();

            failureConnection.Open();
            const string updateFailedValidation = "update IceaLionMarine set UCR_Validation=@fail where RowID =@id";
            var sqlCommand = new SqlCommand(updateFailedValidation, failureConnection);
            sqlCommand.Parameters.AddWithValue("@fail", "fail");
            sqlCommand.Parameters.AddWithValue("@id", id);
            sqlCommand.ExecuteNonQuery();
            failureConnection.Close();
        }

        public static void UpdateSuccessValidationOperation(long id)
        {
            var updateSubmissionConnection = Connection.GetConnections();
            updateSubmissionConnection.Open();
            const string updateForSubmission =
                "update IceaLionMarine set UCR_Validation=@success, Updated=@updated where RowID =@id";
            var sqlCommand = new SqlCommand(updateForSubmission,
                updateSubmissionConnection);
            sqlCommand.Parameters.AddWithValue("@success", "Successful");
            sqlCommand.Parameters.AddWithValue("@updated", 1);
            sqlCommand.Parameters.AddWithValue("@id", id);
            sqlCommand.ExecuteNonQuery();
            updateSubmissionConnection.Close();
        }

        public static void UpdateSuccessfulSubmissionOperation(SubmissionResponse response,long id)
        {
            var sqlConnection = Connection.GetConnections();
            sqlConnection.Open();
            const string updateFailedValidation =
                "update IceaLionMarine set UCR_Validation=@success,Submission_Status=@subStatus,code=@code,description=@description, Updated=@updated  where RowID=@id";
            var sqlCommand = new SqlCommand(updateFailedValidation, sqlConnection);
            sqlCommand.Parameters.AddWithValue("@success", "Successful");
            sqlCommand.Parameters.AddWithValue("@subStatus", "Successful");
            sqlCommand.Parameters.AddWithValue("@code", response.mcires.mci_response.errors.error.code);
            sqlCommand.Parameters.AddWithValue("@description", response.mcires.mci_response.errors.error.description);
            sqlCommand.Parameters.AddWithValue("@updated", 1);
            sqlCommand.Parameters.AddWithValue("@id", id);
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }
        
        public static void UpdateFailureOnAmendmentOperation(long id)
        {
            var failureConnection = Connection.GetConnections();

            failureConnection.Open();
            const string updateFailedValidation = "update IceaLionMarine set Submission_Status=@fail where RowID =@id";
            var sqlCommand = new SqlCommand(updateFailedValidation, failureConnection);
            sqlCommand.Parameters.AddWithValue("@fail", "Amendment Failed");
            sqlCommand.Parameters.AddWithValue("@id", id);
            sqlCommand.ExecuteNonQuery();
            failureConnection.Close();
        }
        
        public static void UpdateFailureOnCancellationOperation(long id)
        {
            var failureConnection = Connection.GetConnections();
            failureConnection.Open();
            const string updateFailedValidation = "update IceaLionMarine set Submission_Status=@fail where RowID =@id";
            var sqlCommand = new SqlCommand(updateFailedValidation, failureConnection);
            sqlCommand.Parameters.AddWithValue("@fail", "Cancellation Failed");
            sqlCommand.Parameters.AddWithValue("@id", id);
            sqlCommand.ExecuteNonQuery();
            failureConnection.Close();
        }
        
        public static void UpdateSuccessfulAmendmentOperation(SubmissionResponse response,long id)
        {
            var sqlConnection = Connection.GetConnections();
            sqlConnection.Open();
            const string updateFailedValidation =
                "update IceaLionMarine set UCR_Validation=@success,Submission_Status=@subStatus,code=@code,description=@description, Updated=@updated  where RowID=@id";
            var sqlCommand = new SqlCommand(updateFailedValidation, sqlConnection);
            sqlCommand.Parameters.AddWithValue("@success", "Successful");
            sqlCommand.Parameters.AddWithValue("@subStatus", "Amendment Successful");
            sqlCommand.Parameters.AddWithValue("@code", response.mcires.mci_response.errors.error.code);
            sqlCommand.Parameters.AddWithValue("@description", response.mcires.mci_response.errors.error.description);
            sqlCommand.Parameters.AddWithValue("@updated", 1);
            sqlCommand.Parameters.AddWithValue("@id", id);
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }
        
        public static void UpdateSuccessfulCancellationOperation(SubmissionResponse response,long id)
        {
            var sqlConnection = Connection.GetConnections();
            sqlConnection.Open();
            const string updateFailedValidation =
                "update IceaLionMarine set UCR_Validation=@success,Submission_Status=@subStatus,code=@code,description=@description, Updated=@updated  where RowID=@id";
            var sqlCommand = new SqlCommand(updateFailedValidation, sqlConnection);
            sqlCommand.Parameters.AddWithValue("@success", "Successful");
            sqlCommand.Parameters.AddWithValue("@subStatus", "Cancellation Successful");
            sqlCommand.Parameters.AddWithValue("@code", response.mcires.mci_response.errors.error.code);
            sqlCommand.Parameters.AddWithValue("@description", response.mcires.mci_response.errors.error.description);
            sqlCommand.Parameters.AddWithValue("@updated", 1);
            sqlCommand.Parameters.AddWithValue("@id", id);
            sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }
        public static void UpdateStatusBusyOnValidation(long id)
        {
            var conn2 = Connection.GetConnections();
            conn2.Open();
            const string updateValidation ="update IceaLionMarine set UCR_Validation=@busy where RowID =@id";
            var sqlCommand = new SqlCommand(updateValidation, conn2);
            sqlCommand.Parameters.AddWithValue("@busy", "busy");
            sqlCommand.Parameters.AddWithValue("@id", id);
            sqlCommand.ExecuteNonQuery();
            conn2.Close();
        }

        public static void UpdateDatabaseForValidation(IceaLionMarine marine, UcrValidationResponse response)
        {
            //do UpdateValidation 
            using (var updateSuccessfulValidation = Connection.GetConnections())
            {
                try
                {
                    updateSuccessfulValidation.Open();
                    //set stored procedure name
                    const string spMarineMciValidationUpdate = @"dbo.[spMarineMCI_Validation_Update]";

                    //define the SqlCommand object
                    var cmd = new SqlCommand(spMarineMciValidationUpdate, updateSuccessfulValidation);
                    //Set SqlParameter - the employee id parameter value will be set from the command line
                    cmd.Parameters.Add("@RowID", SqlDbType.BigInt).Value = marine.RowID;
                    cmd.Parameters.Add("@ItmUk", SqlDbType.BigInt).Value = marine.ItmUk;
                    cmd.Parameters.Add("@UCR_No", SqlDbType.VarChar).Value =
                        response.DocumentDetails.ucr_no;
                    cmd.Parameters.Add("@UCR_Validation", SqlDbType.VarChar).Value = "Successful";
                    cmd.Parameters.Add("@UCR_ReferenceNo", SqlDbType.VarChar).Value = response.DocumentHeader.refno;
                    cmd.Parameters.Add("@msgid", SqlDbType.VarChar).Value = response.DocumentHeader.msgid;
                    cmd.Parameters.Add("@refno", SqlDbType.VarChar).Value =
                        response.DocumentHeader.refno;
                    cmd.Parameters.Add("@msg_func", SqlDbType.VarChar).Value =
                        response.DocumentHeader.msg_func;
                    cmd.Parameters.Add("@sender", SqlDbType.VarChar).Value =
                        response.DocumentHeader.sender;
                    cmd.Parameters.Add("@receiver", SqlDbType.VarChar).Value =
                        response.DocumentHeader.receiver;
                    cmd.Parameters.Add("@version", SqlDbType.VarChar).Value =
                        response.DocumentHeader.version;
                    cmd.Parameters.Add("@docno", SqlDbType.VarChar).Value =
                        response.DocumentHeader.docno;
                    cmd.Parameters.Add("@docgroup", SqlDbType.VarChar).Value =
                        response.DocumentHeader.docgroup;
                    cmd.Parameters.Add("@msgdate", SqlDbType.VarChar).Value =
                        response.DocumentHeader.msgdate;
                    cmd.Parameters.Add("@rstatus", SqlDbType.VarChar).Value =
                        response.DocumentDetails.status;
                    cmd.Parameters.Add("@ImporterPIN", SqlDbType.VarChar).Value =
                        response.DocumentDetails.ImporterPIN;
                    cmd.Parameters.Add("@ImporterName", SqlDbType.VarChar).Value =
                        response.DocumentDetails.ImporterName;
                    cmd.Parameters.Add("@ExporterPIN", SqlDbType.VarChar).Value =
                        response.DocumentDetails.ExporterPIN;
                    cmd.Parameters.Add("@ExporterName", SqlDbType.VarChar).Value =
                        response.DocumentDetails.ExporterName;
                    cmd.Parameters.Add("@CustomsEntryNumber", SqlDbType.VarChar).Value = response.DocumentDetails.CustomsEntryNumber ?? "CODE-GEN";
                    cmd.Parameters.Add("@partshipmentInd", SqlDbType.VarChar).Value =
                        response.DocumentDetails.partshipmentInd ?? "CODE-GEN";
                    cmd.Parameters.Add("@parentUCR", SqlDbType.VarChar).Value =
                        response.DocumentDetails.parentUCR ?? "CODE-GEN";
                    cmd.Parameters.Add("@code", SqlDbType.VarChar).Value = "";
                    cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = "";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                    SubmissionLogger.Log(" Updated Successfully ");
                    updateSuccessfulValidation.Close();
                }
                catch (Exception e)
                {
                    SubmissionLogger.Log(e);
                }
            }
        }
    }
}