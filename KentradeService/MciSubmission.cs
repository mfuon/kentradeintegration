using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using KentradeService.Utils;
using KenTradeService.Data;
using KenTradeService.Models;
using KenTradeService.Utils;
using Newtonsoft.Json;

namespace KenTradeService
{
    public static class MciSubmission
    {
        public static void MciValidationAndSubmissionAction()
        {
            try
            {
                //sql connection object
                using (var conn = Connection.GetConnections())
                {
                    //set stored procedure name
                    var spMarineSelect = @"dbo.[spMarineMCI_Select]";
                    var cmdMarineSelect = new SqlCommand(spMarineSelect, conn);
                    conn.Open();
                    cmdMarineSelect.CommandType = CommandType.StoredProcedure;
                    var dataReader = cmdMarineSelect.ExecuteReader();
                    SubmissionLogger.Log(Constants.Lines());
                    SubmissionLogger.Log("READING THE DATABASE....");
                    SubmissionLogger.Log(Constants.Lines());
                    //check if there are records
                    try
                    {
                        if (dataReader.HasRows)
                            while (dataReader.Read())
                            {
                                var marine = new IceaLionMarine
                                {
                                    LogNo = dataReader.GetInt64(0),
                                    ItmUk = dataReader.GetInt64(1),
                                    RowID = dataReader.GetInt64(2),
                                    Request_number = dataReader.GetString(3),
                                    Ver_no = dataReader.GetDecimal(4),
                                    Broker_Name = dataReader.GetString(7),
                                    ucr_no = dataReader.GetString(9),
                                    Ucr_code = dataReader.GetString(9),
                                    msgid = "894389438934984398",
                                    msg_func = "1",
                                    sender = "ION",
                                    receiver = "KESWS",
                                    version = "1"
                                };

                                var ucr_request_token = WebRequestUtil.sha256_hash(marine.ucr_no + "IONION2017");
                                SubmissionLogger.Log("Id : " + marine.RowID + " Broker Name : " +
                                                     marine.Broker_Name + " UCRNO : " + marine.ucr_no + " TOKEN : " +
                                                     ucr_request_token);
                                if (marine.RowID <= 0) continue;
                                try
                                {
                                    //UPDATE THE TABLE IceaLionMarine TO HAVE A UCR_VALIDATION ON A BUSY STATE TO AVOID ERRORS ON AN OPERATION THAT IS IN PROGRESS
                                    DbOperations.UpdateStatusBusyOnValidation(marine.RowID);
                                    //do MciSubmission
                                    var soapEnvelopeXml =
                                        WebRequestUtil.CreateUcrValidationSoapEnvelope(marine, ucr_request_token);
                                    var webRequest = WebRequestUtil.CreateWebRequest(Constants.Url, Constants.Action);
                                    WebRequestUtil.InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);
                                    var asyncResult = webRequest.BeginGetResponse(null, null);
                                    asyncResult.AsyncWaitHandle.WaitOne();
                                    // get the response from the completed web request.
                                    string soapResult;
                                    using (var webResponse = webRequest.EndGetResponse(asyncResult))
                                    {
                                        using (var rd =
                                            new StreamReader(webResponse.GetResponseStream() ?? throw new Exception()))
                                        {
                                            soapResult = rd.ReadToEnd();
                                        }

                                        Console.Write(soapResult);
                                    }

                                    var response = JsonUtils.GetValidationResponse(soapResult);
                                    var json = JsonConvert.SerializeObject(response);
                                    SubmissionLogger.Log("Json response >> --- >> " + json);

                                    if (response.DocumentDetails.status == "VA" ||
                                        response.DocumentDetails.status.Equals("VA"))
                                    {
                                        //Executing ValidationUpdate Stored Procedure

                                        DbOperations.UpdateDatabaseForValidation(marine, response);

                                        //do MciSubmission
                                        var submissionConnection = Connection.GetConnections();
                                        //set stored procedure name
                                        var spMarineSelectForSubmission = @"dbo.[spMarineMCI_Submission_Select]";
                                        var cmdMarineSelectForSubmission = new SqlCommand(spMarineSelectForSubmission,
                                            submissionConnection);
                                        submissionConnection.Open();
                                        cmdMarineSelectForSubmission.CommandType = CommandType.StoredProcedure;
                                        var dataReaderForSubmission = cmdMarineSelectForSubmission.ExecuteReader();
                                        SubmissionLogger.Log("RETRIEVING SUBMISSION DATA...");
                                        //check if there are records
                                        GetSubmissionData(dataReaderForSubmission);
                                        //close data reader
                                        dataReaderForSubmission.Close();

                                        //close connection
                                        submissionConnection.Close();
                                    }
                                    else
                                    {
                                        DbOperations.UpdateFailureOperation(marine.RowID);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    SubmissionLogger.Log(ex);
                                    DbOperations.UpdateFailureOperation(marine.RowID);
                                }
                            }
                        else
                            SubmissionLogger.Log("NO RECORDS FOUND");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($@"An Error has occured {ex.Message}");
                        SubmissionLogger.Log($@"An Error has occured {ex.Message}");
                    }
                    

                    //close data reader
                    dataReader.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                SubmissionLogger.Log(ex);
            }
        }

        private static void GetSubmissionData(SqlDataReader dataReaderForSubmission)
        {
            if (dataReaderForSubmission.HasRows)
                while (dataReaderForSubmission.Read())
                    try
                    {
                        var marineForSubmission = new IceaLionMarineSubmission
                        {
                            LogNo = dataReaderForSubmission.GetInt64(0),
                            ItmUk = dataReaderForSubmission.GetInt64(1),
                            RowID = dataReaderForSubmission.GetInt64(2),
                            Request_number = dataReaderForSubmission.GetString(3),
                            Ver_no = dataReaderForSubmission.GetDecimal(4),
                            ImporterPIN = dataReaderForSubmission.GetString(5),
                            Importer_Type = dataReaderForSubmission.GetString(6).Equals("Indirect", StringComparison.OrdinalIgnoreCase) ? "InDirect"
                                : "Direct",
                            Broker_Name = dataReaderForSubmission.GetString(7),
                            Broker_PIN = dataReaderForSubmission.GetString(8),
                            Ucr_code = dataReaderForSubmission.GetString(9),
                            Origin_country = dataReaderForSubmission.GetString(10),
                            Destination_country = dataReaderForSubmission.GetString(11),
                            Origin_port = dataReaderForSubmission.GetString(12),
                            Destination_port = dataReaderForSubmission.GetString(13),
                            Mci_Issuer = dataReaderForSubmission.GetString(14),
                            Rotation_Number = dataReaderForSubmission.GetString(15),
                            Vessel_Name = dataReaderForSubmission.GetString(16),
                            Voyage_Number = dataReaderForSubmission.GetString(17),
                            eta = dataReaderForSubmission.GetString(18),
                            dry_bulk_ind = dataReaderForSubmission.GetString(19),
                            cont_ind = dataReaderForSubmission.GetString(20),
                            general_ind = dataReaderForSubmission.GetString(21),
                            vehicle_ind = dataReaderForSubmission.GetString(22),
                            animal_ind = dataReaderForSubmission.GetString(23),
                            liquid_ind = dataReaderForSubmission.GetString(24),
                            Financier_PIN = dataReaderForSubmission.GetString(25),
                            Tran_port = dataReaderForSubmission.GetString(26),
                            Tran_country = dataReaderForSubmission.GetString(27),
                            Tran_quantity = dataReaderForSubmission.GetDouble(28) == double.Parse("0") ? double.Parse("1") : dataReaderForSubmission.GetDouble(28),
                            Tran_vessel = dataReaderForSubmission.GetString(29),
                            itemSeq_num = dataReaderForSubmission.GetInt32(30),
                            goods_desc = dataReaderForSubmission.GetString(31),
                            quantity = dataReaderForSubmission.GetInt32(32),
                            uom = dataReaderForSubmission.GetString(33),
                            package_type = dataReaderForSubmission.GetString(34),
                            Marks_Numbers = dataReaderForSubmission.GetString(35),
                            item_Origin_country = dataReaderForSubmission.GetString(36),
                            item_currency = dataReaderForSubmission.GetString(37),
                            CIF = dataReaderForSubmission.GetDouble(38),
                            CIF_NCY = dataReaderForSubmission.GetDouble(39),
                            Doc_name = dataReaderForSubmission.GetString(40),
                            Doc_code = dataReaderForSubmission.GetString(41),
                            File_Name = dataReaderForSubmission.GetString(42),
                            Mci_internal_number = dataReaderForSubmission.GetString(43),
                            Mci_certificate_number = dataReaderForSubmission.GetString(44),
                            Policy_Number = dataReaderForSubmission.GetString(45),
                            Premium_amount = dataReaderForSubmission.GetDecimal(46),
                            Policy_holders_fund = dataReaderForSubmission.GetDecimal(47),
                            Stamp_duty = dataReaderForSubmission.GetDecimal(48),
                            Training_levy = dataReaderForSubmission.GetDecimal(49),
                            Total_premium = dataReaderForSubmission.GetDecimal(50),
                            Sum_Insured = dataReaderForSubmission.GetDecimal(51),
                            Servey_agents = dataReaderForSubmission.GetString(52),
                            approval_type = dataReaderForSubmission.GetString(53),
                            role_code = dataReaderForSubmission.GetString(54),
                            Remarks = dataReaderForSubmission.GetString(55),
                            Status = dataReaderForSubmission.GetString(56),
                            Created_date = dataReaderForSubmission.GetString(57) /*,
                                                                Transferee_PIN = dataReaderForSubmission.GetString(58),
                                                                Transferee_UCR = dataReaderForSubmission.GetString(59)*/
                        };


                        SubmissionLogger.Log(Constants.Lines());
                        var ucr_request_token_for_submission = WebRequestUtil.sha256_hash(marineForSubmission.RowID + "ION");
                        SubmissionLogger.Log(
                            "Id : " + marineForSubmission.RowID + " FINERNCER PIN : " +
                            marineForSubmission.Financier_PIN + " STATUS : " +
                            marineForSubmission.Status + " TOKEN : " +
                            ucr_request_token_for_submission);
                        SubmissionLogger.Log(Constants.Lines());
                        if (marineForSubmission.RowID <= 0) continue;
                        try
                        {
                            DbOperations.UpdateSuccessValidationOperation(marineForSubmission
                                .RowID);

                            var soapEnvelopeXmlForSubmission =
                                WebRequestUtil.CreateUcrSubmissionSoapEnvelope(marineForSubmission,
                                    ucr_request_token_for_submission);


                            var webRequestForSubmission =
                                WebRequestUtil.CreateSubmissionWebRequest(Constants.SubmissionUrl,
                                    Constants.SubmissionAction);
                            WebRequestUtil.InsertSubmissionSoapEnvelopeIntoWebRequest(
                                soapEnvelopeXmlForSubmission, webRequestForSubmission);
                            var asyncResultForSubmission =
                                webRequestForSubmission.BeginGetResponse(null, null);
                            asyncResultForSubmission.AsyncWaitHandle.WaitOne();
                            // get the response from the completed web request.
                            string soapResultForSubmission;
                            using (var webResponse =
                                webRequestForSubmission.EndGetResponse(asyncResultForSubmission)
                            )
                            {
                                using (var rd = new StreamReader(
                                    webResponse.GetResponseStream() ?? throw new Exception()))
                                {
                                    soapResultForSubmission = rd.ReadToEnd();
                                }
                            }

                            try
                            {
                                var submissionResponse = JsonUtils.GetSubmissionResponse(soapResultForSubmission);
                                var status = submissionResponse.mcires.mci_response.response.code;
                                if (status.Equals("F"))
                                {
                                    SubmissionLogger.Log("FAIL" + Constants.Lines());
                                    SubmissionLogger.Log(submissionResponse.mcires.ToString());
                                    DbOperations.UpdateFailureOperation(marineForSubmission.RowID);
                                    SubmissionLogger.Log("END FAIL" + Constants.Lines());
                                }
                                else
                                {
                                    SubmissionLogger.Log("SUCCESS" + Constants.Lines());
                                    SubmissionLogger.Log(submissionResponse.mcires.ToString());
                                    DbOperations.UpdateSuccessfulSubmissionOperation(submissionResponse,marineForSubmission.RowID);
                                    SubmissionLogger.Log("END SUCCESS" + Constants.Lines());
                                }
                            }
                            catch (Exception e)
                            {
                                SubmissionLogger.Log("******SEE ERROR LOG******");
                                SubmissionLogger.Log(e);
                            }
                        }
                        catch (Exception ex)
                        {
                            SubmissionLogger.Log(ex);
                        }
                    }
                    catch (Exception exception)
                    {
                        SubmissionLogger.Log(exception);
                    }
            else
                SubmissionLogger.Log("NO RECORDS FOUND");
        }
    }
}