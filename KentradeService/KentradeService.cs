﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using KentradeService.Utils;
using KenTradeService.Utils;

namespace KenTradeService
{
    public partial class MCI_KENTRADE_SERVICE_001 : ServiceBase
    {
        //Declare a Timer
        private Timer _timer;

        public MCI_KENTRADE_SERVICE_001()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _timer = new Timer { Interval = Constants.ServiceTimer };
            _timer.Elapsed += KenTradeMciOperationsService;
            _timer.Enabled = true;
            Constants.Initializer();
        }

        private void KenTradeMciOperationsService(object sender, ElapsedEventArgs e)
        {
            /*MciSubmission.MciValidationAndSubmissionAction();
            MciAmendment.MciValidationAndAmendment();*/
            Parallel.Invoke(
                MciSubmission.MciValidationAndSubmissionAction,
                MciAmendment.MciValidationAndAmendment,
                MciCancellation.MciCancellationAction
                );
        }

        protected override void OnStop()
        {
            Constants.End();
        }


    }
}