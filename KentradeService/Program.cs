﻿using System.ServiceProcess;
namespace KenTradeService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var servicesToRun = new ServiceBase[]
            {
                new MCI_KENTRADE_SERVICE_001()
            };
            ServiceBase.Run(servicesToRun);
        }
    }
}
