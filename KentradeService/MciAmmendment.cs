using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using KentradeService.Utils;
using KenTradeService.Data;
using KenTradeService.Models;
using KenTradeService.Utils;

namespace KenTradeService
{
    public static class MciAmendment
    {
        public static void MciValidationAndAmendment()
        {
            //Do Ucr Validation for amending the submission
            using (var conn = Connection.GetConnections())
            {
                //-----------------------------------
                //get data from Amendments
                const string spMarineMciAmendmentSelect = @"dbo.[spMarineMCI_Amendment_Select]";
                var cmdAmendments = new SqlCommand(spMarineMciAmendmentSelect, conn);
                conn.Open();
                cmdAmendments.CommandType = CommandType.StoredProcedure;
                var dataReader = cmdAmendments.ExecuteReader();
                AmendmentLogger.Log(Constants.Lines());
                AmendmentLogger.Log("READING THE DATABASE....");
                AmendmentLogger.Log(Constants.Lines());
                //check if there are records
                //------------------------------------

                if (dataReader.HasRows)
                    while (dataReader.Read())
                    {
                        var marine = new IceaLionMarine
                        {
                            LogNo = dataReader.GetInt64(0),
                            ItmUk = dataReader.GetInt64(1),
                            RowID = dataReader.GetInt64(2),
                            Request_number = dataReader.GetString(3),
                            Ver_no = dataReader.GetDecimal(4),
                            Broker_Name = dataReader.GetString(7),
                            ucr_no = dataReader.GetString(9),
                            Ucr_code = dataReader.GetString(9)
                        };

                        var marine2 = new IceaLionMarineSubmission
                        {
                            LogNo = dataReader.GetInt64(0),
                            ItmUk = dataReader.GetInt64(1),
                            RowID = dataReader.GetInt64(2),
                            Request_number = dataReader.GetString(3),
                            Ver_no = dataReader.GetDecimal(4),
                            ImporterPIN = dataReader.GetString(5),
                            Importer_Type = dataReader.GetString(6),
                            Broker_Name = dataReader.GetString(7),
                            Broker_PIN = dataReader.GetString(8),
                            Ucr_code = dataReader.GetString(9),
                            Origin_country = dataReader.GetString(10),
                            Destination_country = dataReader.GetString(11),
                            Origin_port = dataReader.GetString(12),
                            Destination_port = dataReader.GetString(13),
                            Mci_Issuer = dataReader.GetString(14),
                            Rotation_Number = dataReader.GetString(15),
                            Vessel_Name = dataReader.GetString(16),
                            Voyage_Number = dataReader.GetString(17),
                            eta = dataReader.GetString(18),
                            dry_bulk_ind = dataReader.GetString(19),
                            cont_ind = dataReader.GetString(20),
                            general_ind = dataReader.GetString(21),
                            vehicle_ind = dataReader.GetString(22),
                            animal_ind = dataReader.GetString(23),
                            liquid_ind = dataReader.GetString(24),
                            Financier_PIN = dataReader.GetString(25),
                            Tran_port = dataReader.GetString(26),
                            Tran_country = dataReader.GetString(27),
                            Tran_quantity = dataReader.GetDouble(28),
                            Tran_vessel = dataReader.GetString(29),
                            itemSeq_num = dataReader.GetInt32(30),
                            goods_desc = dataReader.GetString(31),
                            quantity = dataReader.GetInt32(32),
                            uom = dataReader.GetString(33),
                            package_type = dataReader.GetString(34),
                            Marks_Numbers = dataReader.GetString(35),
                            item_Origin_country = dataReader.GetString(36),
                            item_currency = dataReader.GetString(37),
                            CIF = dataReader.GetDouble(38),
                            CIF_NCY = dataReader.GetDouble(39),
                            Doc_name = dataReader.GetString(40),
                            Doc_code = dataReader.GetString(41),
                            File_Name = dataReader.GetString(42),
                            Mci_internal_number = dataReader.GetString(43),
                            Mci_certificate_number = dataReader.GetString(44),
                            Policy_Number = dataReader.GetString(45),
                            Premium_amount = dataReader.GetDecimal(46),
                            Policy_holders_fund = dataReader.GetDecimal(47),
                            Stamp_duty = dataReader.GetDecimal(48),
                            Training_levy = dataReader.GetDecimal(49),
                            Total_premium = dataReader.GetDecimal(50),
                            Sum_Insured = dataReader.GetDecimal(51),
                            Servey_agents = dataReader.GetString(52),
                            approval_type = dataReader.GetString(53),
                            role_code = dataReader.GetString(54),
                            Remarks = dataReader.GetString(55),
                            Status = dataReader.GetString(56),
                            Created_date = dataReader.GetString(57)
                        };

                        using (var thisConnection = Connection.GetConnections())
                        {
                            try
                            {
                                thisConnection.Open();
                                //-----------------------------------
                                //get data from Successful Submissions
                                const string spMarineSelect = @"dbo.[spMarineMCI_Get_Ucr_Submission_Request_Number]";
                                var cmdMarineSelect = new SqlCommand(spMarineSelect, thisConnection);
                                cmdMarineSelect.Parameters.Add("@Updated", SqlDbType.Int).Value = 1;
                                cmdMarineSelect.Parameters.Add("@Ucr_code", SqlDbType.VarChar).Value = marine.Ucr_code;
                                cmdMarineSelect.Parameters.Add("@Submission_Status", SqlDbType.VarChar).Value =
                                    "Successful";
                                cmdMarineSelect.CommandType = CommandType.StoredProcedure;
                                var amendmentsDataReader = cmdMarineSelect.ExecuteReader();
                                //
                                AmendmentLogger.Log(Constants.Lines());
                                AmendmentLogger.Log("GETTING SUBMISSION REQUEST NUMBER DETAILS ");
                                AmendmentLogger.Log(Constants.Lines());
                                //check if there are records
                                //------------------------------------
                                while (amendmentsDataReader.Read())
                                {
                                    AmendmentLogger.Log("REQUEST NUMBER  : " + amendmentsDataReader.GetString(0));
                                    AmendmentLogger.Log("UCR NUMBER      : " + marine.Ucr_code);
                                    //do MciAmendment
                                    GetAmendmentsData(marine2, amendmentsDataReader.GetString(0));
                                }
                            }
                            catch (Exception e)
                            {
                                AmendmentLogger.Log(e);
                            }
                        }
                    }

                //close data reader
                dataReader.Close();
                //close connection
                conn.Close();
            }
        }

        private static void GetAmendmentsData(IceaLionMarineSubmission marineSubmission, string refNumber)
        {
            try
            {                
                try
                {
                    var marineForSubmission = marineSubmission;
                   AmendmentLogger.Log(Constants.Lines());
                    var ucrRequestTokenForSubmission =
                        WebRequestUtil.sha256_hash(marineForSubmission.RowID + "ION");
                    AmendmentLogger.Log(
                        "Id : " + marineForSubmission.RowID + " FINANCIER PIN : " +
                        marineForSubmission.Ucr_code + " STATUS : " +
                        marineForSubmission.Status + " TOKEN : " +
                        ucrRequestTokenForSubmission);
                    AmendmentLogger.Log(Constants.Lines());

                    try
                    {
                        DbOperations.UpdateSuccessValidationOperation(marineForSubmission
                            .RowID);

                        var soapEnvelopeXmlForSubmission =
                            WebRequestUtil.CreateUcrAmendmentSoapEnvelope(marineForSubmission,
                                ucrRequestTokenForSubmission, refNumber);


                        var webRequestForSubmission =
                            WebRequestUtil.CreateSubmissionWebRequest(Constants.SubmissionUrl,
                                Constants.SubmissionAction);
                        WebRequestUtil.InsertSubmissionSoapEnvelopeIntoWebRequest(
                            soapEnvelopeXmlForSubmission, webRequestForSubmission);
                        var asyncResultForSubmission =
                            webRequestForSubmission.BeginGetResponse(null, null);
                        asyncResultForSubmission.AsyncWaitHandle.WaitOne();
                        // get the response from the completed web request.
                        string soapResultForSubmission;
                        using (var webResponse =
                            webRequestForSubmission.EndGetResponse(asyncResultForSubmission)
                        )
                        {
                            using (var rd = new StreamReader(
                                webResponse.GetResponseStream() ?? throw new Exception()))
                            {
                                soapResultForSubmission = rd.ReadToEnd();
                            }
                        }

                        try
                        {
                            var submissionResponse = JsonUtils.GetAmendmentResponse(soapResultForSubmission);
                            var status = submissionResponse.mcires.mci_response.response.code;
                            if (status.Equals("F"))
                            {
                                AmendmentLogger.Log(Constants.Lines());
                                AmendmentLogger.Log("*******START FAIL************");
                                AmendmentLogger.Log(submissionResponse.mcires.mci_response.errors.error.description );
                                DbOperations.UpdateFailureOnAmendmentOperation(marineForSubmission.RowID);
                                AmendmentLogger.Log("*******END FAIL********" + Constants.Lines());
                                AmendmentLogger.Log(Constants.Lines());
                            }
                            else
                            {
                                AmendmentLogger.Log(Constants.Lines());
                                AmendmentLogger.Log("*******START SUCCESS********");
                                AmendmentLogger.Log(submissionResponse.mcires.mci_response.errors.error.description );
                                DbOperations.UpdateSuccessfulAmendmentOperation(submissionResponse,
                                    marineForSubmission.RowID);
                                AmendmentLogger.Log("*******END OF SUCCESS********");
                                AmendmentLogger.Log(Constants.Lines());
                            }
                        }
                        catch (Exception e)
                        {
                            AmendmentLogger.Log("******SEE ERROR LOG******");
                            AmendmentLogger.Log(e);
                        }
                    }
                    catch (Exception ex)
                    {
                        AmendmentLogger.Log(ex);
                    }
                }
                catch (Exception exception)
                {
                    AmendmentLogger.Log(exception);
                }
            }
            catch (Exception e)
            {
                AmendmentLogger.Log("NO RECORDS FOUND");
                AmendmentLogger.Log(e);
            }
        }
    }
}